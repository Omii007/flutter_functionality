import 'package:flutter/material.dart';
import 'package:pageview_builder_scroll/scroll.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ScrollImage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
