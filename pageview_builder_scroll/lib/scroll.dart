import 'dart:async';

import 'package:flutter/material.dart';

class ScrollImage extends StatefulWidget {
  const ScrollImage({super.key});

  @override
  State createState() => _ScrollImageState();
}

class _ScrollImageState extends State {
  @override
  void initState() {
    super.initState();
    startTimer();
  }

  final PageController _pageController = PageController(initialPage: 0);
  int _activeImage = 0;
  Timer? _timer;

  void startTimer() {
    _timer = Timer.periodic(const Duration(seconds: 3), (timer) {
      if (_pageController.page == imageList.length - 1) {
        // check if it's on the last page
        _pageController.animateToPage(
          0,
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      } else {
        _pageController.nextPage(
          duration: const Duration(milliseconds: 500),
          curve: Curves.easeInOut,
        );
      }
    });
  }

  List<String> imageList = [
    "assets/images/DSC02585.JPG",
    "assets/images/DSC02586.JPG",
    "assets/images/DSC_1175.JPG",
    "assets/images/DSC_1528 (1).JPG",
    "assets/images/DSC_1538 (1).JPG",
    "assets/images/DSC_9002.JPG",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "PageView Builder",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          // Stack Widget is used for overlap the 2 widget
          Stack(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height / 4,
                width: double.infinity,
                child: PageView.builder(
                    controller: _pageController,
                    itemCount: imageList.length,
                    onPageChanged: (value) {
                      setState(() {
                        _activeImage = value;
                      });
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return Image.asset(
                        imageList[index],
                        fit: BoxFit.cover,
                        filterQuality: FilterQuality.high,
                      );
                    }),
              ),
              // code for page indicator
              // Psitioned is used for overlaped widget position show
              Positioned(
                bottom: 10,
                left: 0,
                right: 0,
                child: Container(
                  color: Colors.transparent,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children:
                        // show the length of list for mapping future operation
                        List<Widget>.generate(
                      imageList.length,
                      (index) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 5),
                        child: InkWell(
                          // used for tap on circleavatar go to those image
                          onTap: () {
                            _pageController.animateToPage(
                              index,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.easeIn,
                            );
                          },
                          // used for in list how many data are stored show using circle
                          child: CircleAvatar(
                            radius: _activeImage == index ? 8 : 4,
                            backgroundColor: _activeImage == index
                                ? Colors.black
                                : Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
