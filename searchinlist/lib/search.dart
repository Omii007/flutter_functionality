
import 'package:flutter/material.dart';

class SearchData extends StatefulWidget {
  const SearchData({super.key});

  @override
  State createState()=> _SearchDataState();
}

class _SearchDataState extends State {

  List<Map<String,dynamic>> playerList = [
    // 1
    {
      'name': "Virat Kohli",
      'image':"https://w0.peakpx.com/wallpaper/196/34/HD-wallpaper-virat-kohli-in-blue-jersey-virat-kohli-blue-indian-cricket-king-kohli-sports.jpg",
      'role': "Batsman",
    },
    // 2
    {
      'name': "Rohit Sharma",
      'image':"https://w0.peakpx.com/wallpaper/196/34/HD-wallpaper-virat-kohli-in-blue-jersey-virat-kohli-blue-indian-cricket-king-kohli-sports.jpg",
      'role': "Batsman",
    },
    // 3
    {
      'name': "Shubman Gill",
      'image':"https://w0.peakpx.com/wallpaper/196/34/HD-wallpaper-virat-kohli-in-blue-jersey-virat-kohli-blue-indian-cricket-king-kohli-sports.jpg",
      'role': "Batsman",
    },
    // 4
    {
      'name': "Yashasvi Jaiswal",
      'image':"https://w0.peakpx.com/wallpaper/196/34/HD-wallpaper-virat-kohli-in-blue-jersey-virat-kohli-blue-indian-cricket-king-kohli-sports.jpg",
      'role': "Batsman",
    },
    // 5
    {
      'name': "KL Rahul",
      'image':"https://i.pinimg.com/originals/04/ea/6f/04ea6ffa7d01b77d8d79dbda9cdccc25.jpg",
      'role': "Batsman",
    },
    // 6
    {
      'name': "MS Dhoni",
      'image':"https://i.ndtvimg.com/i/2017-09/mahendra-singh-dhoni-afp_806x605_41504452160.jpg",
      'role': "Batsman",
    },
    // 7
    {
      'name': "Shreyas Iyer",
      'image':"https://rapidkings.com/wp-content/uploads/2023/05/7-1664206606.jpg",
      'role': "Batsman",
    },
    // 8
    {
      'name': "Hardik Pandya",
      'image':"https://toppng.com/uploads/preview/hardik-pandya-india-bcci-cricket-odi-icc-champions-hardik-pandya-11563176115osoe1y1o8g.png",
      'role': "Batsman",
    },
    // 9
    {
      'name': "Ishan Kishan",
      'image':"https://circleofcricket.com/post_image/post_image_d1ceefa.jpg",
      'role': "Batsman",
    },
    // 10
    {
      'name': "Yuvraj Sing",
      'image':"https://m.media-amazon.com/images/M/MV5BMjQ5OTY4MjYtMjBkNi00ZWFjLWI1YmItMTkxZTk0ODYwNDZiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg",
      'role': "Batsman",
    },
  ];

  List<Map<String,dynamic>> dummyList = [];

  @override
  void initState() {
    super.initState();
    // copy original list in dummy list and work on dummy list
    dummyList = playerList;
  }

  void searchInfo(String value){
    List<Map<String,dynamic>> result = [];
    
    if(value.isEmpty){
      // when search field is empty then all data can show 
      result = playerList;
    }else{
      result = playerList.where((user) => user['name'].toLowerCase().contains(value.toLowerCase())).toList();
    }
    setState(() {
      dummyList = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Search In List",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: [
            TextField(
              onChanged: (value) {
                searchInfo(value);
              },
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  onPressed: (){}, 
                  icon: const Icon(
                    Icons.search_rounded,
                    size: 35,
                  ),
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30),
                  borderSide: const BorderSide(
                    color: Colors.black,
                  ),
                ),
                hintText: "Search",
                hintStyle: const TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 20,
                  color: Color.fromRGBO(0,0,0,0.7),
                ),
              ),
            ),

            const SizedBox(
              height: 20,
            ),

            Expanded(
              child: dummyList.isNotEmpty?
              ListView.builder(
                itemCount: dummyList.length,
                itemBuilder: (BuildContext context,int index){
                  return Card(
                    elevation: 1,
                    margin: const EdgeInsets.symmetric(vertical: 2),
                    child: ListTile(
                      leading: CircleAvatar(
                        radius: 30,
                        child: Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(image: NetworkImage(dummyList[index]['image']),
                              fit: BoxFit.fitHeight,
                              filterQuality: FilterQuality.high
                            )
                          ),
                        ),
                      ),
                      title: Text(dummyList[index]['name']),
                      subtitle: Text(dummyList[index]['role']),
                    ),
                  );
                }): const Center(
                  child: Text("No result found",
                    style: TextStyle(
                      fontWeight: FontWeight.normal,
                      fontSize: 15,
                      color: Colors.black,
                    ),
                  ),
                ),
            )
          ],
        ),
      ),
    );
  }
}