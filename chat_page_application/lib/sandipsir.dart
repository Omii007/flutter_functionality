import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final TextEditingController _controller = TextEditingController();
  final List<String> _messages = [];
  WebSocketChannel? _channel;

  @override
  void initState() {
    super.initState();
    _connectWebSocket();
  }

  void _connectWebSocket() {
    // Connect to the registration WebSocket
    final registrationChannel = WebSocketChannel.connect(
      Uri.parse('ws://13.201.237.163:8087/app/chat.register'),
    );

    registrationChannel.stream.listen((message) {
      print('Registration response: $message');
      registrationChannel.sink.close();

      // After registration, connect to the public topic
      _channel = WebSocketChannel.connect(
        Uri.parse('ws://13.201.237.163:8087/topic/public'),
      );

      _channel!.stream.listen((message) {
        setState(() {
          _messages.add('Received: $message');
        });
      });
    });

    // Send a registration message (you might need to adjust this based on the expected format)
    registrationChannel.sink.add(json.encode({
      'type': 'register',
      'username': 'User${DateTime.now().millisecondsSinceEpoch}'
    }));
  }

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      final sendChannel = WebSocketChannel.connect(
        Uri.parse('ws://13.201.237.163:8087/app/chat.send'),
      );

      sendChannel.sink.add(json.encode({
        'type': 'message',
        'content': _controller.text,
      }));

      setState(() {
        _messages.add('Sent: ${_controller.text}');
        _controller.clear();
      });

      sendChannel.sink.close();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Flutter Chat App')),
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: _messages.length,
              itemBuilder: (context, index) {
                return ListTile(title: Text(_messages[index]));
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _controller,
                    decoration: InputDecoration(labelText: 'Send a message'),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send),
                  onPressed: _sendMessage,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _channel?.sink.close();
    _controller.dispose();
    super.dispose();
  }
}
