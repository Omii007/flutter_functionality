import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'localdatabase_screen.dart';

class FireBaseChat extends StatefulWidget {
  final ChatUser loggedInUser;
  final String? clubId;

  const FireBaseChat({
    required this.loggedInUser,
    super.key,
    this.clubId,
  });

  @override
  State createState() => _FireBaseChatState();
}

class _FireBaseChatState extends State<FireBaseChat> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  CollectionReference _messagesCollection = FirebaseFirestore.instance
      .collection('messages')
      .doc('0')
      .collection('club');
  StreamSubscription? _messageSubscription;
  List<ChatMessage> _messages = [];
  bool _isLoading = true;

  final ScrollController _scrollController = ScrollController();
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    // loggedInUser = ChatUser(id: widget.loggedInUser.toString());
    _messagesCollection = FirebaseFirestore.instance
        .collection('messages')
        .doc('${widget.clubId}')
        .collection('club');

    receiveMessage();
    receiveMetadata();
    _loadMessagesFromDatabase();
    print("UserId : ${widget.loggedInUser.id}");
  }

  Future<void> _loadMessagesFromDatabase() async {
    final dbMessages = await DatabaseHelper.instance.getMessages();
    // print("Local DataBase : ${dbMessages[0].user.firstName}");
    setState(() {
      _messages = dbMessages;
      _isLoading = false;
    });
    print("Real DataBase : ${_messages.map((e) => e.text)}");
  }

  void receiveMessage() {
    _messageSubscription = _messagesCollection
        .orderBy('createdAt', descending: true)
        .snapshots()
        .listen((snapShot) async {
      final receivedMessages = snapShot.docs.map((doc) {
        final data = doc.data() as Map<String, dynamic>;
        print("Data : ${data['user_id']}");
        return ChatMessage(
          text: data['text'],
          user: data['user_id'] == widget.loggedInUser.id
              ? widget.loggedInUser
              : ChatUser(
                  firstName: data['user_name'],
                  id: data['user_id'],
                ),
          createdAt:
              (data['createdAt'] as Timestamp?)?.toDate() ?? DateTime.now(),
        );
      }).toList();

      // Update local messages and save to the database
      for (var message in receivedMessages) {
        await DatabaseHelper.instance
            .insertMessage(message, widget.clubId ?? '');
      }

      setState(() {
        _messages = receivedMessages;
        _isLoading = false;
      });

      // Update the createdAt timestamp for the club
    });
  }

  Future<void> receiveMetadata() async {
    FirebaseFirestore.instance
        .collection('messages')
        .doc('${widget.clubId}')
        .snapshots()
        .listen((value) async {
      final data = value.data();
      // Safely retrieve and handle `createdAt` field
      final Timestamp? createdAtTimestamp = data?['createdAt'];
      final DateTime? createdAt = createdAtTimestamp?.toDate();

      print("Metadata CreatedAt: $createdAt");

      await DatabaseHelper.instance.insertOrUpdateClubMetadata(
          value.data()?['bookClubId'], (createdAt ?? DateTime.now()));
    });
  }

  // void _loadMessages() {
  //   _messagesCollection
  //       .orderBy('createdAt', descending: true)
  //       .snapshots()
  //       .listen((snapshot) {
  //     setState(() {
  //       _messages = snapshot.docs.map((doc) {
  //         final data = doc.data() as Map<String, dynamic>;
  //         return ChatMessage(
  //           text: data['text'],
  //           user: data['user_id'] == widget.loggedInUser.id
  //               ? widget.loggedInUser
  //               : ChatUser(id: data['user_id'], firstName: data['user_name']),
  //           createdAt: data['createdAt'] != null
  //               ? (data['createdAt'] as Timestamp).toDate()
  //               : DateTime.now(), // or handle `null` in another appropriate way
  //         );
  //       }).toList();
  //       _isLoading = false;
  //     });
  //   });
  // }

  void onSend(ChatMessage message) async {
    final newMessage = {
      'text': message.text,
      'user_id': widget.loggedInUser.id,
      'user_name': widget.loggedInUser.firstName,
      'createdAt': FieldValue.serverTimestamp(),
    };
    await _messagesCollection.add(newMessage);

    /// FOR SEND NOTIFICATION TO OTHER USERS

    FirebaseFirestore.instance
        .collection('messages')
        .doc('${widget.clubId}')
        .set(
      {
        'createdAt': DateTime.now(),
        'bookClubId': widget.clubId,
      },
      SetOptions(merge: true),
    );

    final doc = FirebaseFirestore.instance
        .collection('messages')
        .doc('${widget.clubId}');

    // .doc(widget.loggedInUser.id);

    await doc.set({
      'users': {
        "userId${widget.loggedInUser.id}": {
          'fcmToken': '${widget.loggedInUser.id} FCMTOKEN',
        }
      },
    }, SetOptions(merge: true));

    _scrollToBottom();
  }

  void _scrollToBottom() {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(
        _scrollController.position.minScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    _messageSubscription?.cancel();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 245, 214, 1),
      appBar: AppBar(
        title: const Text('Firebase Chat'),
      ),
      body: _isLoading
          ? const Center(child: CircularProgressIndicator())
          : Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/Bg_CrinkledPaper (1).png'),
                  fit: BoxFit.cover,
                  filterQuality: FilterQuality.high,
                ),
              ),
              child: DashChat(
                inputOptions: InputOptions(
                  textController: _controller,
                  sendButtonBuilder: (send) {
                    return IconButton(
                      onPressed: send,
                      icon: const Icon(
                        Icons.send,
                        color: Color.fromRGBO(15, 137, 86, 1),
                        size: 20,
                      ),
                    );
                  },
                  inputToolbarPadding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  inputToolbarMargin: const EdgeInsets.only(top: 10),
                  inputToolbarStyle: const BoxDecoration(
                    color: Color.fromRGBO(255, 245, 214, 0),
                    border: Border(
                      top: BorderSide(
                        color: Colors.black,
                        width: 1.5,
                      ),
                    ),
                  ),
                  alwaysShowSend: true,
                  showTraillingBeforeSend: true,
                  cursorStyle: const CursorStyle(
                    color: Color.fromRGBO(124, 156, 154, 1),
                  ),
                  inputMaxLines: 5,
                  inputDecoration: InputDecoration(
                    isDense: true,
                    contentPadding: const EdgeInsets.all(10),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    filled: true,
                    fillColor: const Color.fromRGBO(255, 245, 214, 0),
                  ),
                ),
                messageListOptions: MessageListOptions(
                  scrollController: _scrollController,
                  dateSeparatorBuilder: (date) {
                    final now = DateTime.now();
                    final today = DateTime(now.year, now.month, now.day);
                    final messageDate =
                        DateTime(date.year, date.month, date.day);
                    final difference = today.difference(messageDate).inDays;

                    String formattedDate;
                    if (difference == 0) {
                      formattedDate = 'Today';
                    } else if (difference == 1) {
                      formattedDate = 'Yesterday';
                    } else {
                      formattedDate =
                          DateFormat('EEE, MMM dd, yyyy').format(date);
                    }

                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(formattedDate),
                      ),
                    );
                  },
                ),
                messageOptions: MessageOptions(
                  messagePadding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  messageDecorationBuilder:
                      (message, previousMessage, nextMessage) {
                    final isOutgoing =
                        message.user.id == widget.loggedInUser.id;
                    return BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: isOutgoing
                          ? const Color.fromRGBO(124, 156, 154, 1)
                          : Colors.white,
                      border: Border.all(
                        color: const Color.fromRGBO(37, 57, 67, 1),
                      ),
                    );
                  },
                  userNameBuilder: (user) {
                    return Column(
                      children: [
                        Text(
                          user.firstName ?? '',
                          style: const TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(height: 5),
                      ],
                    );
                  },
                  showCurrentUserAvatar: false,
                  showOtherUsersAvatar: true,
                  showOtherUsersName: true,
                  showTime: true,
                  currentUserContainerColor:
                      const Color.fromRGBO(124, 156, 154, 1),
                  messageTextBuilder: (message, previousMessage, nextMessage) {
                    final date =
                        DateFormat('hh:mm a').format(message.createdAt);
                    final isOutgoing =
                        message.user.id == widget.loggedInUser.id;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          message.text,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontSize: 16,
                            color: isOutgoing
                                ? Colors.white
                                : const Color.fromRGBO(37, 57, 67, 1),
                          ),
                        ),
                        const SizedBox(height: 4),
                        Text(
                          date,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 12,
                            color: isOutgoing
                                ? Colors.white
                                : const Color.fromRGBO(37, 57, 67, 1),
                          ),
                        ),
                      ],
                    );
                  },
                ),
                currentUser: widget.loggedInUser,
                onSend: (ChatMessage message) {
                  onSend(message);
                },
                messages: _messages,
              ),
            ),
    );
  }
}








// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat_2/dash_chat_2.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';

// class FireBaseChat extends StatefulWidget {
//   final ChatUser loggedInUser; // Pass in the current user at login

//   const FireBaseChat({required this.loggedInUser, super.key});

//   @override
//   State createState() => _FireBaseChatState();
// }

// class _FireBaseChatState extends State<FireBaseChat> {
//   final FirebaseFirestore _firestore = FirebaseFirestore.instance;
//   final CollectionReference _messagesCollection =
//       FirebaseFirestore.instance.collection('messages');

//   Stream<List<ChatMessage>> getMessages() {
//     return _messagesCollection
//         .orderBy('createdAt', descending: true)
//         .snapshots()
//         .map(
//       (snapshot) {
//         return snapshot.docs.map((doc) {
//           final data = doc.data() as Map<String, dynamic>;
//           return ChatMessage(
//             text: data['text'],
//             user: data['user_id'] == widget.loggedInUser.id
//                 ? widget.loggedInUser
//                 : ChatUser(id: data['user_id'], firstName: data['user_name']),
//             createdAt: (data['createdAt'] as Timestamp).toDate(),
//           );
//         }).toList();
//       },
//     );
//   }

//   void onSend(ChatMessage message) async {
//     final newMessage = {
//       'text': message.text,
//       'user_id': widget.loggedInUser.id,
//       'user_name': widget.loggedInUser.firstName,
//       'createdAt': FieldValue.serverTimestamp(),
//     };
//     await _messagesCollection.add(newMessage);
//     _scrollToBottom();
//   }

//   List<ChatMessage> messages = <ChatMessage>[];
//   final ScrollController _scrollController = ScrollController();
//   final TextEditingController _controller = TextEditingController();

//   @override
//   void dispose() {
//     _controller.dispose();
//     _scrollController.dispose();
//     super.dispose();
//   }

//   void _scrollToBottom() {
//     if (_scrollController.hasClients) {
//       _scrollController.animateTo(
//         _scrollController.position.minScrollExtent,
//         duration: const Duration(milliseconds: 300),
//         curve: Curves.easeOut,
//       );
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: const Color.fromRGBO(255, 245, 214, 1),
//       appBar: AppBar(
//         title: const Text('Firebase Chat'),
//       ),
//       body: StreamBuilder<List<ChatMessage>>(
//           stream: getMessages(),
//           builder: (context, snapshot) {
//             if (snapshot.connectionState == ConnectionState.waiting) {
//               return Center(child: CircularProgressIndicator());
//             }

//             return Container(
//               decoration: const BoxDecoration(
//                 // color: Color.fromRGBO(37, 57, 67, 1),
//                 image: DecorationImage(
//                   image: AssetImage(
//                     'assets/Bg_CrinkledPaper (1).png',
//                   ),
//                   fit: BoxFit.cover,
//                   filterQuality: FilterQuality.high,
//                 ),
//               ),
//               child: DashChat(
//                 inputOptions: InputOptions(
//                   textController: _controller,
//                   sendButtonBuilder: (send) {
//                     return IconButton(
//                       onPressed: send,
//                       icon: const Icon(
//                         Icons.send,
//                         color: Color.fromRGBO(15, 137, 86, 1),
//                         size: 20,
//                       ),
//                     );
//                   },
//                   inputToolbarPadding:
//                       const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//                   inputToolbarMargin: const EdgeInsets.only(top: 10),
//                   inputToolbarStyle: const BoxDecoration(
//                     color: Color.fromRGBO(255, 245, 214, 0),
//                     border: Border(
//                       top: BorderSide(
//                         color: Colors.black,
//                         width: 1.5,
//                       ),
//                     ),
//                   ),
//                   alwaysShowSend: true,
//                   showTraillingBeforeSend: true,
//                   cursorStyle: const CursorStyle(
//                     color: Color.fromRGBO(124, 156, 154, 1),
//                   ),
//                   inputMaxLines: 5,
//                   inputDecoration: InputDecoration(
//                     isDense: true,
//                     contentPadding: const EdgeInsets.all(10),
//                     focusedBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         width: 1.5,
//                         color: Color.fromRGBO(37, 57, 67, 1),
//                       ),
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                     border: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         width: 1.5,
//                         color: Color.fromRGBO(37, 57, 67, 1),
//                       ),
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                     enabledBorder: OutlineInputBorder(
//                       borderSide: const BorderSide(
//                         width: 1.5,
//                         color: Color.fromRGBO(37, 57, 67, 1),
//                       ),
//                       borderRadius: BorderRadius.circular(15),
//                     ),
//                     filled: true,
//                     fillColor: const Color.fromRGBO(255, 245, 214, 0),
//                   ),
//                 ),
//                 messageListOptions: MessageListOptions(
//                   scrollController: _scrollController,
//                   dateSeparatorBuilder: (date) {
//                     final now = DateTime.now();
//                     final today = DateTime(now.year, now.month, now.day);
//                     final messageDate =
//                         DateTime(date.year, date.month, date.day);
//                     final difference = today.difference(messageDate).inDays;

//                     String formattedDate;
//                     if (difference == 0) {
//                       formattedDate = 'Today';
//                     } else if (difference == 1) {
//                       formattedDate = 'Yesterday';
//                     } else {
//                       formattedDate =
//                           DateFormat('EEE, MMM dd, yyyy').format(date);
//                     }

//                     return Center(
//                       child: Padding(
//                         padding: const EdgeInsets.all(10.0),
//                         child: Text(formattedDate),
//                       ),
//                     );
//                   },
//                 ),
//                 messageOptions: MessageOptions(
//                   messagePadding:
//                       const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
//                   messageDecorationBuilder:
//                       (message, previousMessage, nextMessage) {
//                     final isOutgoing =
//                         message.user.id == widget.loggedInUser.id;
//                     return BoxDecoration(
//                       borderRadius: BorderRadius.circular(10),
//                       color: isOutgoing
//                           ? const Color.fromRGBO(124, 156, 154, 1)
//                           : Colors.white,
//                       border: Border.all(
//                         color: const Color.fromRGBO(37, 57, 67, 1),
//                       ),
//                     );
//                   },
//                   userNameBuilder: (user) {
//                     return Column(
//                       children: [
//                         Text(
//                           user.firstName ?? '',
//                           style: const TextStyle(
//                             fontSize: 12,
//                             color: Colors.black,
//                           ),
//                         ),
//                         const SizedBox(height: 5),
//                       ],
//                     );
//                   },
//                   showCurrentUserAvatar: false,
//                   showOtherUsersAvatar: true,
//                   showOtherUsersName: true,
//                   showTime: true,
//                   currentUserContainerColor:
//                       const Color.fromRGBO(124, 156, 154, 1),
//                   messageTextBuilder: (message, previousMessage, nextMessage) {
//                     final date =
//                         DateFormat('hh:mm a').format(message.createdAt);
//                     final isOutgoing =
//                         message.user.id == widget.loggedInUser.id;
//                     return Column(
//                       crossAxisAlignment: CrossAxisAlignment.start,
//                       children: [
//                         Text(
//                           message.text,
//                           textAlign: TextAlign.left,
//                           style: TextStyle(
//                             fontSize: 16,
//                             color: isOutgoing
//                                 ? Colors.white
//                                 : const Color.fromRGBO(37, 57, 67, 1),
//                           ),
//                         ),
//                         const SizedBox(height: 4),
//                         Text(
//                           date,
//                           textAlign: TextAlign.end,
//                           style: TextStyle(
//                             fontSize: 12,
//                             color: isOutgoing
//                                 ? Colors.white
//                                 : const Color.fromRGBO(37, 57, 67, 1),
//                           ),
//                         ),
//                       ],
//                     );
//                   },
//                 ),
//                 currentUser: widget.loggedInUser,
//                 onSend: (ChatMessage message) {
//                   onSend(message);
//                 },
//                 messages: snapshot.data?.toList() ?? [],
//               ),
//             );
//           }),
//     );
//   }
// }



// // import 'package:cloud_firestore/cloud_firestore.dart';
// // import 'package:dash_chat_2/dash_chat_2.dart';
// // import 'package:flutter/material.dart';
// // import 'package:intl/intl.dart';

// // class FireBaseChat extends StatefulWidget {
// //   const FireBaseChat({super.key});
// //   @override
// //   State createState() => _FireBaseChatState();
// // }

// // class _FireBaseChatState extends State<FireBaseChat> {
// //   final FirebaseFirestore _firestore = FirebaseFirestore.instance;
// //   final CollectionReference _messagesCollection =
// //       FirebaseFirestore.instance.collection('messages');

// //   Stream<List<ChatMessage>> getMessages() {
// //     return _messagesCollection
// //         .orderBy('createdAt', descending: true)
// //         .snapshots()
// //         .map(
// //       (snapshot) {
// //         return snapshot.docs.map((doc) {
// //           final data = doc.data() as Map<String, dynamic>;
// //           return ChatMessage(
// //             text: data['text'],
// //             user: data['user_id'] == currentUser.id
// //                 ? currentUser
// //                 : otherUser.first,
// //             createdAt: (data['createdAt'] as Timestamp).toDate(),
// //           );
// //         }).toList();
// //       },
// //     );
// //   }

// //   void onSend(ChatMessage message) async {
// //     final newMessage = {
// //       'text': message.text,
// //       'user_id': message.user.id,
// //       'createdAt': FieldValue.serverTimestamp(),
// //     };
// //     await _messagesCollection.add(newMessage);
// //     _scrollToBottom();
// //   }

// //   final ChatUser currentUser = ChatUser(
// //     id: '1',
// //     firstName: 'Charles',
// //     lastName: 'Leclerc',
// //   );

// //   final otherUser = [
// //     ChatUser(
// //         id: '2',
// //         firstName: 'Max',
// //         lastName: 'Verstappen',
// //         profileImage:
// //             "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
// //     // ChatUser(
// //     //     id: '3',
// //     //     firstName: 'Maroti',
// //     //     lastName: 'Verstappen',
// //     //     profileImage:
// //     //         "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
// //     // ChatUser(
// //     //     id: '4',
// //     //     firstName: 'Omkar',
// //     //     lastName: 'Verstappen',
// //     //     profileImage:
// //     //         "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
// //   ];

// //   List<ChatMessage> messages = <ChatMessage>[];
// //   final ScrollController _scrollController = ScrollController();
// //   final TextEditingController _controller = TextEditingController();

// //   @override
// //   void initState() {
// //     // connect();

// //     messages = [
// //       ChatMessage(
// //         text: 'Hello! How are you?',
// //         user: otherUser[0],
// //         createdAt: DateTime.now().subtract(Duration(days: 1)),
// //       ),
// //       ChatMessage(
// //         text: 'I’m good, thanks! How about you?',
// //         user: currentUser,
// //         createdAt: DateTime.now().subtract(Duration(days: 1)),
// //       ),
// //       // ChatMessage(
// //       //   text: 'Doing great! Thanks ',
// //       //   user: otherUser[1],
// //       //   createdAt: DateTime.now(),
// //       // ),
// //       // ChatMessage(
// //       //   text: 'Doing great!',
// //       //   user: otherUser[2],
// //       //   createdAt: DateTime.now(),
// //       // ),
// //     ];
// //     // connect();
// //     super.initState();
// //   }

// //   @override
// //   void dispose() {
// //     _controller.dispose();
// //     _scrollController.dispose();
// //     super.dispose();
// //   }

// //   void _scrollToBottom() {
// //     if (_scrollController.hasClients) {
// //       _scrollController.animateTo(
// //         _scrollController.position.minScrollExtent,
// //         duration: const Duration(milliseconds: 300),
// //         curve: Curves.easeOut,
// //       );
// //     }
// //   }

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       backgroundColor: const Color.fromRGBO(255, 245, 214, 1),
// //       appBar: AppBar(
// //         title: const Text('Firebase chat'),
// //       ),
// //       body: StreamBuilder<List<ChatMessage>>(
// //           stream: getMessages(),
// //           builder: (context, snapshot) {
// //             if (!snapshot.hasData)
// //               return Center(child: CircularProgressIndicator());

// //             return DashChat(
// //               // scrollController: _scrollController,
// //               inputOptions: InputOptions(
// //                 textController: _controller,
// //                 sendButtonBuilder: (send) {
// //                   return IconButton(
// //                     onPressed: send,
// //                     icon: const Icon(
// //                       Icons.send,
// //                       color: Color.fromRGBO(37, 57, 67, 1),
// //                     ),
// //                   );
// //                 },
// //                 alwaysShowSend: true,
// //                 showTraillingBeforeSend: true,
// //                 cursorStyle:
// //                     const CursorStyle(color: Color.fromRGBO(124, 156, 154, 1)),
// //                 // sendOnEnter: true,
// //                 inputMaxLines: 5,

// //                 inputDecoration: InputDecoration(
// //                   isDense: true,
// //                   contentPadding: EdgeInsets.all(10),
// //                   focusedBorder: OutlineInputBorder(
// //                     borderSide: const BorderSide(
// //                       width: 1.5,
// //                       color: Color.fromRGBO(37, 57, 67, 1),
// //                     ),
// //                     borderRadius: BorderRadius.circular(10),
// //                   ),
// //                   enabledBorder: OutlineInputBorder(
// //                     borderSide: const BorderSide(
// //                       width: 1.5,
// //                       color: Color.fromRGBO(37, 57, 67, 1),
// //                     ),
// //                     borderRadius: BorderRadius.circular(10),
// //                   ),
// //                   border: OutlineInputBorder(
// //                     borderSide: const BorderSide(
// //                       width: 1.5,
// //                       color: Color.fromRGBO(37, 57, 67, 1),
// //                     ),
// //                     borderRadius: BorderRadius.circular(10),
// //                   ),
// //                   filled: true,
// //                   fillColor: const Color.fromRGBO(255, 245, 214, 1),
// //                 ),
// //               ),
// //               messageListOptions: MessageListOptions(
// //                 scrollController: _scrollController,
// //                 dateSeparatorBuilder: (date) {
// //                   final data = DateFormat('MMM dd, yyyy').format(date);
// //                   return Center(
// //                     child: Padding(
// //                       padding: const EdgeInsets.all(10.0),
// //                       child: Text(data),
// //                     ),
// //                   );
// //                 },
// //               ),
// //               messageOptions: MessageOptions(
// //                 onLongPressMessage: (p0) {},
// //                 messagePadding:
// //                     const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
// //                 messageDecorationBuilder:
// //                     (message, previousMessage, nextMessage) {
// //                   final isOutgoing = message.user.id == currentUser.id;
// //                   return BoxDecoration(
// //                     borderRadius: BorderRadius.circular(4),
// //                     color: isOutgoing
// //                         ? const Color.fromRGBO(124, 156, 154, 1)
// //                         : Colors.white,
// //                     border: Border.all(
// //                       color: const Color.fromRGBO(37, 57, 67, 1),
// //                     ),
// //                   );
// //                 },
// //                 userNameBuilder: (user) {
// //                   return Column(
// //                     children: [
// //                       Text(
// //                         user.firstName ?? '',
// //                         textAlign: TextAlign.start,
// //                         style: const TextStyle(
// //                           fontSize: 14,
// //                           color: Colors.black,
// //                         ),
// //                       ),
// //                       const SizedBox(
// //                         height: 5,
// //                       ),
// //                     ],
// //                   );
// //                 },
// //                 showCurrentUserAvatar: false,
// //                 showOtherUsersAvatar: true,
// //                 showOtherUsersName: true,
// //                 containerColor: Colors.white,
// //                 showTime: true,
// //                 currentUserContainerColor:
// //                     const Color.fromRGBO(124, 156, 154, 1),
// //                 messageTextBuilder: (message, previousMessage, nextMessage) {
// //                   final date = DateFormat('hh:mm a').format(message.createdAt);
// //                   final isOutgoing = message.user.id == currentUser.id;
// //                   return Column(
// //                     crossAxisAlignment: CrossAxisAlignment.start,
// //                     children: [
// //                       Text(
// //                         message.text,
// //                         textAlign: TextAlign.start,
// //                         style: TextStyle(
// //                           fontSize: 16,
// //                           color: isOutgoing
// //                               ? Colors.white
// //                               : const Color.fromRGBO(37, 57, 67, 1),
// //                         ),
// //                       ),
// //                       const SizedBox(
// //                         height: 4,
// //                       ),
// //                       Text(
// //                         date,
// //                         textAlign: TextAlign.start,
// //                         style: TextStyle(
// //                           fontSize: 12,
// //                           color: isOutgoing
// //                               ? Colors.white
// //                               : const Color.fromRGBO(37, 57, 67, 1),
// //                         ),
// //                       ),
// //                     ],
// //                   );
// //                 },
// //               ),
// //               currentUser: currentUser,

// //               onSend: (ChatMessage m) {
// //                 setState(() {
// //                   onSend(m);
// //                   print("Message : ${m.text}");
// //                   // sendMessage(m);
// //                   // sendMessage(m.text, 1, 2);
// //                   // messages.add(m);
// //                   _scrollToBottom();
// //                 });
// //               },
// //               messages: messages.reversed.toList(),
// //             );
// //           }),
// //     );
// //   }

// //   void send(ChatMessage m) {
// //     setState(() {
// //       messages.add(m);
// //       _scrollToBottom();
// //     });
// //   }
// // }
