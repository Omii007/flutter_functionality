import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  // Use WebSocket channel (without SockJS)
  final WebSocketChannel channel = WebSocketChannel.connect(
    Uri.parse('ws://13.201.237.163:8087/websocket'), // Use WebSocket URL
  );

  final TextEditingController _controller = TextEditingController();

  // List to hold all messages sent and received
  List<String> messages = [];

  void _sendMessage() {
    if (_controller.text.isNotEmpty) {
      // Send message to the WebSocket server
      channel.sink.add(_controller.text);

      // Add the message to the list of messages and update the UI
      setState(() {
        messages.add("You: ${_controller.text}");
      });

      // Clear the text field
      _controller.clear();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat App'),
      ),
      body: Column(
        children: [
          Expanded(
            child: StreamBuilder(
              stream: channel.stream,
              builder: (context, snapshot) {
                // Add received message to the list and update the UI
                if (snapshot.hasData) {
                  messages.add("Server: ${snapshot.data}");
                }

                return ListView.builder(
                  itemCount: messages.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(messages[index]),
                    );
                  },
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    controller: _controller,
                    decoration: InputDecoration(hintText: 'Enter message'),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send),
                  onPressed: _sendMessage,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    channel.sink.close(); // Close the WebSocket connection
    super.dispose();
  }
}


// import 'package:flutter/material.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';

// class ChatScreen extends StatefulWidget {
//   @override
//   _ChatScreenState createState() => _ChatScreenState();
// }

// class _ChatScreenState extends State<ChatScreen> {
//   // Replace with your actual WebSocket URL
//   final WebSocketChannel channel = WebSocketChannel.connect(
//     Uri.parse('ws://13.201.237.163:8087/websocket'),
//   );

//   final TextEditingController _controller = TextEditingController();

//   // List to hold all messages sent and received
//   List<String> messages = [];

//   void _sendMessage() {
//     if (_controller.text.isNotEmpty) {
//       // Send message to the WebSocket server
//       channel.sink.add(_controller.text);

//       // Add the message to the list of messages and update the UI
//       setState(() {
//         messages.add("You: ${_controller.text}");
//       });

//       // Clear the text field
//       _controller.clear();
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Chat App'),
//       ),
//       body: Column(
//         children: [
//           Expanded(
//             child: StreamBuilder(
//               stream: channel.stream,
//               builder: (context, snapshot) {
//                 // Add received message to the list and update the UI
//                 if (snapshot.hasData) {
//                   messages.add("Server: ${snapshot.data}");
//                 }

//                 return ListView.builder(
//                   itemCount: messages.length,
//                   itemBuilder: (context, index) {
//                     return ListTile(
//                       title: Text(messages[index]),
//                     );
//                   },
//                 );
//               },
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Row(
//               children: [
//                 Expanded(
//                   child: TextField(
//                     controller: _controller,
//                     decoration: InputDecoration(hintText: 'Enter message'),
//                   ),
//                 ),
//                 IconButton(
//                   icon: Icon(Icons.send),
//                   onPressed: _sendMessage,
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   @override
//   void dispose() {
//     channel.sink.close();
//     super.dispose();
//   }
// }


// // import 'package:flutter/material.dart';
// // import 'package:web_socket_channel/web_socket_channel.dart';

// // class ChatScreen extends StatefulWidget {
// //   @override
// //   _ChatScreenState createState() => _ChatScreenState();
// // }

// // class _ChatScreenState extends State<ChatScreen> {
// //   // Replace with your actual WebSocket URL
// //   final WebSocketChannel channel = WebSocketChannel.connect(
// //     Uri.parse('ws://13.201.237.163:8087/websocket'),
// //   );

// //   final TextEditingController _controller = TextEditingController();

// //   void _sendMessage() {
// //     if (_controller.text.isNotEmpty) {
// //       sendMessage.add(_controller.text);
// //       channel.sink.add(_controller.text);
// //       _controller.clear();
// //     }
// //   }

// //   List<String> sendMessage = [];

// //   @override
// //   Widget build(BuildContext context) {
// //     return Scaffold(
// //       appBar: AppBar(
// //         title: Text('Chat App'),
// //       ),
// //       body: Column(
// //         children: [
// //           Expanded(
// //             child: StreamBuilder(
// //               stream: channel.stream,
// //               builder: (context, snapshot) {
// //                 if (snapshot.hasError) {
// //                   return Center(child: Text('Connection Error'));
// //                 } else if (!snapshot.hasData) {
// //                   return Center(child: Text('No messages yet'));
// //                 } else {
// //                   return ListView(
// //                     children: [
// //                       ListTile(
// //                         title: Text(sendMessage[0]),
// //                       ),
// //                     ],
// //                   );
// //                 }
// //               },
// //             ),
// //           ),
// //           Padding(
// //             padding: const EdgeInsets.all(8.0),
// //             child: Row(
// //               children: [
// //                 Expanded(
// //                   child: TextField(
// //                     controller: _controller,
// //                     decoration: InputDecoration(hintText: 'Enter message'),
// //                   ),
// //                 ),
// //                 IconButton(
// //                   icon: Icon(Icons.send),
// //                   onPressed: _sendMessage,
// //                 ),
// //               ],
// //             ),
// //           ),
// //         ],
// //       ),
// //     );
// //   }

// //   @override
// //   void dispose() {
// //     channel.sink.close();
// //     super.dispose();
// //   }
// // }
