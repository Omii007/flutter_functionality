import 'dart:io';

import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/web_socket_channel.dart';

class Basic extends StatefulWidget {
  const Basic({super.key});
  @override
  State createState() => _BasicState();
}

class _BasicState extends State<Basic> {
  final ChatUser currentUser = ChatUser(
    id: '1',
    firstName: 'Charles',
    lastName: 'Leclerc',
  );

  final otherUser = [
    ChatUser(
        id: '2',
        firstName: 'Max',
        lastName: 'Verstappen',
        profileImage:
            "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
    // ChatUser(
    //     id: '3',
    //     firstName: 'Maroti',
    //     lastName: 'Verstappen',
    //     profileImage:
    //         "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
    // ChatUser(
    //     id: '4',
    //     firstName: 'Omkar',
    //     lastName: 'Verstappen',
    //     profileImage:
    //         "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg"),
  ];

  List<ChatMessage> messages = <ChatMessage>[];
  final ScrollController _scrollController = ScrollController();
  final TextEditingController _controller = TextEditingController();
  io.Socket? socket;
  IOWebSocketChannel? _channel;
  String connectionStatus = "Connecting...";

  @override
  void initState() {
    // connect();
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      _channel = IOWebSocketChannel.connect(
        Uri.parse(
          'ws://13.201.237.163:8087/websocket',
        ),
      );
      print('stream: ${_channel?.stream}');
      // await receiveMessage();
    });
    messages = [
      ChatMessage(
        text: 'Hello! How are you?',
        user: otherUser[0],
        createdAt: DateTime.now().subtract(Duration(days: 1)),
      ),
      ChatMessage(
        text: 'I’m good, thanks! How about you?',
        user: currentUser,
        createdAt: DateTime.now().subtract(Duration(days: 1)),
      ),
      // ChatMessage(
      //   text: 'Doing great! Thanks ',
      //   user: otherUser[1],
      //   createdAt: DateTime.now(),
      // ),
      // ChatMessage(
      //   text: 'Doing great!',
      //   user: otherUser[2],
      //   createdAt: DateTime.now(),
      // ),
    ];
    // connect();
    super.initState();
  }

  void sendMessage(ChatMessage message) async {
    if (message.text.isNotEmpty) {
      try {
        await _channel?.ready;
        _channel?.sink.add(message.text);
        setState(() {
          messages.add(
            ChatMessage(
              text: message.text,
              user: currentUser,
              createdAt: DateTime.now(),
            ),
          );
        });
        _controller.clear();
        print("Message 1: ${_channel?.toString()}");
      } on SocketException catch (e) {
        // Handle the exception.
        print('socket exception: ${e.toString()}');
      } on WebSocketChannelException catch (e) {
        // Handle the exception.
        print('web socket exception: ${e.toString()}');
      }
    }
  }

  // void receiveMessage() {
  //   channel.stream.listen(
  //     (event) {
  //       print("Receive Message : ${event.toString()}");
  //       messages.add(
  //         ChatMessage(
  //           user: otherUser[1],
  //           createdAt: DateTime.now(),
  //           text: event.toString(),
  //         ),
  //       );
  //       setState(() {});
  //     },
  //     onError: (error) {
  //       print("Error : $error");
  //     },
  //     onDone: () {
  //       print("Websocket connection closed");
  //     },
  //   );
  // }
  // Future receiveMessage() async {
  //   await Future.delayed(
  //     const Duration(seconds: 1),
  //     () => _channel?.stream.listen(
  //       (event) {
  //         if (connectionStatus != "Connected") {
  //           setState(() {
  //             connectionStatus = "Connected";
  //           });
  //         }
  //         print("Receive Message: ${event.toString()}");
  //         if (mounted) {
  //           setState(() {
  //             messages.add(
  //               ChatMessage(
  //                 user: otherUser[
  //                     1], // or any user you want to simulate as the sender
  //                 createdAt: DateTime.now(),
  //                 text: event.toString(),
  //               ),
  //             );
  //           });
  //         }
  //       },
  //       onError: (error) {
  //         setState(() {
  //           connectionStatus = "Connection Error";
  //         });
  //         print("Error: $error");
  //       },
  //       onDone: () {
  //         setState(() {
  //           connectionStatus = "Disconnected";
  //         });
  //         print("WebSocket connection closed.");
  //       },
  //     ),
  //   );
  // }

  @override
  void dispose() {
    _controller.dispose();
    _scrollController.dispose();
    super.dispose();
  }
//192.168.0.106

  // void connect() {
  //   socket = io.io("http://192.168.1.110:5000", <String, dynamic>{
  //     "transports": ["websocket"],
  //     "autoConnect": false,
  //   });
  //   socket?.connect();
  //   socket?.emit("signin", 1); //widget?.sourchat?.id
  //   socket?.onConnect((data) {
  //     print("Connected");
  //     socket?.on("message", (msg) {
  //       print(msg);
  //       setMessage("destination", msg["message"]);
  //     });
  //   });
  //   print(socket?.connected);
  // }

  // void sendMessage(String message, int sourceId, int targetId) {
  //   setMessage("source", message);
  //   print("M : $message");
  //   print("sId : $sourceId");
  //   print("tId : $targetId");
  //   socket?.emit("message",
  //       {"message": message, "sourceId": sourceId, "targetId": targetId});
  // }

  // void setMessage(String type, String message) {
  //   ChatMessage chatMessage = ChatMessage(
  //     text: message,
  //     user: currentUser,
  //     createdAt: DateTime.now(),
  //   );
  //   setState(() {
  //     messages.add(chatMessage);
  //     _scrollToBottom();
  //   });
  // }
  // void setMessage(String type, String message) {
  //   ChatUser user = type == "source"
  //       ? currentUser
  //       : otherUser
  //           .firstWhere((u) => u.id == '2'); // Adjust based on actual user
  //   ChatMessage chatMessage = ChatMessage(
  //     text: message,
  //     user: user,
  //     createdAt: DateTime.now(),
  //   );
  //   setState(() {
  //     messages.add(chatMessage);
  //     _scrollToBottom();
  //   });
  // }

  void _scrollToBottom() {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(
        _scrollController.position.minScrollExtent,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 245, 214, 1),
      appBar: AppBar(
        title: Text('WebSocket: $connectionStatus'),
      ),
      body: StreamBuilder(
          stream: _channel?.stream,
          builder: (context, snapshot) {
            if (!snapshot.hasError) {
              return DashChat(
                // scrollController: _scrollController,
                inputOptions: InputOptions(
                  textController: _controller,
                  sendButtonBuilder: (send) {
                    return IconButton(
                      onPressed: send,
                      icon: const Icon(
                        Icons.send,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                    );
                  },
                  alwaysShowSend: true,
                  showTraillingBeforeSend: true,
                  cursorStyle: const CursorStyle(
                      color: Color.fromRGBO(124, 156, 154, 1)),
                  // sendOnEnter: true,
                  inputMaxLines: 5,

                  inputDecoration: InputDecoration(
                    // suffixIconConstraints: BoxConstraints(maxHeight: 30),
                    // suffixIcon: IconButton(
                    //   onPressed: () {},
                    //   icon: Icon(
                    //     Icons.send_outlined,
                    //   ),
                    // ),
                    isDense: true,
                    contentPadding: EdgeInsets.all(10),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    border: OutlineInputBorder(
                      borderSide: const BorderSide(
                        width: 1.5,
                        color: Color.fromRGBO(37, 57, 67, 1),
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    filled: true,
                    fillColor: const Color.fromRGBO(255, 245, 214, 1),
                  ),
                ),
                messageListOptions: MessageListOptions(
                  scrollController: _scrollController,
                  dateSeparatorBuilder: (date) {
                    final data = DateFormat('MMM dd, yyyy').format(date);
                    return Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(data),
                      ),
                    );
                  },
                ),
                messageOptions: MessageOptions(
                  onLongPressMessage: (p0) {},
                  messagePadding:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  messageDecorationBuilder:
                      (message, previousMessage, nextMessage) {
                    final isOutgoing = message.user.id == currentUser.id;
                    return BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: isOutgoing
                          ? const Color.fromRGBO(124, 156, 154, 1)
                          : Colors.white,
                      border: Border.all(
                        color: const Color.fromRGBO(37, 57, 67, 1),
                      ),
                    );
                  },
                  userNameBuilder: (user) {
                    return Column(
                      children: [
                        Text(
                          user.firstName ?? '',
                          textAlign: TextAlign.start,
                          style: const TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                      ],
                    );
                  },
                  showCurrentUserAvatar: false,
                  showOtherUsersAvatar: true,
                  showOtherUsersName: true,
                  containerColor: Colors.white,
                  showTime: true,
                  currentUserContainerColor:
                      const Color.fromRGBO(124, 156, 154, 1),
                  messageTextBuilder: (message, previousMessage, nextMessage) {
                    final date =
                        DateFormat('hh:mm a').format(message.createdAt);
                    final isOutgoing = message.user.id == currentUser.id;
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          message.text,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 16,
                            color: isOutgoing
                                ? Colors.white
                                : const Color.fromRGBO(37, 57, 67, 1),
                          ),
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Text(
                          date,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 12,
                            color: isOutgoing
                                ? Colors.white
                                : const Color.fromRGBO(37, 57, 67, 1),
                          ),
                        ),
                      ],
                    );
                  },
                ),
                currentUser: currentUser,

                onSend: (ChatMessage m) {
                  setState(() {
                    print("Message : ${m.text}");
                    sendMessage(m);
                    // sendMessage(m.text, 1, 2);
                    // messages.add(m);
                    _scrollToBottom();
                  });
                },
                messages: messages.reversed.toList(),
              );
            } else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }

  void send(ChatMessage m) {
    setState(() {
      messages.add(m);
      _scrollToBottom();
    });
  }
}



// import 'package:dash_chat_2/dash_chat_2.dart';
// import 'package:flutter/material.dart';
// import 'package:intl/intl.dart';
// import 'package:socket_io_client/socket_io_client.dart' as io;

// // import 'model/message_model.dart';

// class Basic extends StatefulWidget {
//   const Basic({super.key});
//   @override
//   State createState() => _BasicState();
// }

// class _BasicState extends State<Basic> {
//   final ChatUser currentUser = ChatUser(
//     id: '1',
//     firstName: 'Charles',
//     lastName: 'Leclerc',
//   );

//   final ChatUser otherUser = ChatUser(
//       id: '2',
//       firstName: 'Max',
//       lastName: 'Verstappen',
//       profileImage:
//           "https://images.pexels.com/photos/674010/pexels-photo-674010.jpeg?cs=srgb&dl=pexels-anjana-c-169994-674010.jpg&fm=jpg");
//   ChatUser user = ChatUser(
//     id: '1',
//     firstName: 'Charles',
//     lastName: 'Leclerc',
//   );

//   List<ChatMessage> messages = <ChatMessage>[];
//   ScrollController _scrollController = ScrollController();
//   io.Socket? socket;

//   void connect() {
//     // MessageModel messageModel = MessageModel(sourceId: widget.sourceChat.id.toString(),targetId: );
//     socket = io.io("http://192.168.0.106:5000", <String, dynamic>{
//       "transports": ["websocket"],
//       "autoConnect": false,
//     });
//     socket?.connect();
//     socket?.emit("signin", 1); //widget?.sourchat?.id
//     socket?.onConnect((data) {
//       print("Connected");
//       socket?.on("message", (msg) {
//         print(msg);
//         setMessage("destination", msg["message"]);
//         _scrollController.animateTo(_scrollController.position.maxScrollExtent,
//             duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
//       });
//     });
//     print(socket?.connected);
//   }

//   void sendMessage(String message, int sourceId, int targetId) {
//     setMessage("source", message);
//     socket?.emit("message",
//         {"message": message, "sourceId": sourceId, "targetId": targetId});
//   }

//   void setMessage(String type, String message) {
//     ChatMessage message = ChatMessage(user: user, createdAt: DateTime.now());
//     // MessageModel messageModel = MessageModel(
//     //     type: type,
//     //     message: message,
//     //     time: DateTime.now().toString().substring(10, 16));
//     print(messages);

//     setState(() {
//       messages.add(message);
//     });
//   }

//   @override
//   void initState() {
//     messages = [
//       ChatMessage(
//         text: 'Hello! How are you?',
//         user: otherUser,
//         createdAt: DateTime.now().subtract(Duration(days: 1)),
//       ),
//       ChatMessage(
//         text: 'I’m good, thanks! How about you?',
//         user: currentUser,
//         createdAt: DateTime.now().subtract(Duration(days: 1)),
//       ),
//       ChatMessage(
//         text: 'Doing great! Thanks for asking.',
//         user: otherUser,
//         createdAt: DateTime.now(),
//       ),
//     ];
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: const Color.fromRGBO(255, 245, 214, 1),
//       appBar: AppBar(
//         title: const Text('Basic example'),
//       ),
//       body: DashChat(
        
//         inputOptions: InputOptions(
//           inputDecoration: InputDecoration(
//             focusedBorder: OutlineInputBorder(
//               borderSide: const BorderSide(
//                 width: 1.5,
//                 color: Color.fromRGBO(37, 57, 67, 1),
//               ),
//               borderRadius: BorderRadius.circular(10),
//             ),
//             enabledBorder: OutlineInputBorder(
//               borderSide: const BorderSide(
//                 width: 1.5,
//                 color: Color.fromRGBO(37, 57, 67, 1),
//               ),
//               borderRadius: BorderRadius.circular(10),
//             ),
//             border: OutlineInputBorder(
//               borderSide: const BorderSide(
//                 width: 1.5,
//                 color: Color.fromRGBO(37, 57, 67, 1),
//               ),
//               borderRadius: BorderRadius.circular(10),
//             ),
//             filled: true,
//             fillColor: const Color.fromRGBO(255, 245, 214, 1),
//           ),
//         ),
//         messageListOptions: MessageListOptions(
          
//           dateSeparatorBuilder: (date) {
//             final data = DateFormat('MMM dd, yyyy').format(date);
//             return Center(
//               child: Padding(
//                 padding: const EdgeInsets.all(10.0),
//                 child: Text(data),
//               ),
//             );
//           },
//         ),
//         messageOptions: MessageOptions(
//           messageDecorationBuilder: (message, previousMessage, nextMessage) {
//             final isOutgoing = message.user.id == currentUser.id;
//             return BoxDecoration(
//               borderRadius: BorderRadius.circular(4),
//               color: isOutgoing
//                   ? const Color.fromRGBO(124, 156, 154, 1)
//                   : Colors.white,
//               border: Border.all(
//                 color: const Color.fromRGBO(37, 57, 67, 1),
//               ),
//             );
//           },
//           userNameBuilder: (user) {
//             return Text(
//               user.firstName ?? '',
//               textAlign: TextAlign.start,
//               style: const TextStyle(
//                 fontSize: 10,
//                 color: Colors.black,
//               ),
//             );
//           },

//           showCurrentUserAvatar: false,
//           showOtherUsersAvatar: true,
//           showOtherUsersName: true,
//           containerColor: Colors.white,
//           showTime: true,
//           currentUserContainerColor: const Color.fromRGBO(124, 156, 154, 1),
//           messageTextBuilder: (message, previousMessage, nextMessage) {
//             final date = DateFormat('hh:mm a').format(message.createdAt);
//             final isOutgoing = message.user.id == currentUser.id;
//             return Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               // mainAxisAlignment: MainAxisAlignment.end,
//               children: [
//                 Text(
//                   message.text,
//                   textAlign: TextAlign.start,
//                   style: TextStyle(
//                     fontSize: 14,
//                     color: isOutgoing
//                         ? Colors.white
//                         : const Color.fromRGBO(37, 57, 67, 1),
//                   ),
//                 ),
//                 const SizedBox(
//                   height: 4,
//                 ),
//                 Text(
//                   date,
//                   textAlign: TextAlign.start,
//                   style: TextStyle(
//                     fontSize: 8,
//                     color: isOutgoing
//                         ? Colors.white
//                         : const Color.fromRGBO(37, 57, 67, 1),
//                   ),
//                 ),
//               ],
//             );
//           },
//           // messageTimeBuilder: (message, isOwnMessage) {
//           //   final date = DateFormat('hh:mm a').format(message.createdAt);
//           //   return Padding(
//           //     padding: const EdgeInsets.only(top: 5.0),
//           //     child: Row(
//           //       mainAxisAlignment: MainAxisAlignment.start,
//           //       children: [
//           //         Text(
//           //           date,
//           //           textAlign: TextAlign.start,
//           //           style: const TextStyle(
//           //             fontSize: 8,
//           //             color: Colors.white,
//           //           ),
//           //         ),
//           //       ],
//           //     ),
//           //   );
//           // },
//         ),
        
//         scrollToBottomOptions: ScrollToBottomOptions(),
//         currentUser: currentUser,
//         onSend: (ChatMessage m) {
//           setState(() {
//             messages.insert(0, m);
//           });
//         },
//         messages: messages,
//       ),
//     );
//   }
// }
