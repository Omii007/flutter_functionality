// class MessageModel {
//   final String? type;
//   final String? message;
//   final String? time;
//   MessageModel({this.message, this.type, this.time});
// }

class MessageButtonModel {
  final String? buttonName;
  bool notificationFlag;

  MessageButtonModel({
    this.buttonName,
    this.notificationFlag = false,
  });
}
