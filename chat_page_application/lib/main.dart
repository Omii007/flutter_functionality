// import 'package:flutter/material.dart';
// import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/web_socket_channel.dart';

// void main() {
//   runApp(const ChatApp());
// }

// class ChatApp extends StatelessWidget {
//   const ChatApp({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'WebSocket Chat',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//         useMaterial3: true,
//       ),
//       home: const ChatScreen(),
//     );
//   }
// }

// class ChatScreen extends StatefulWidget {
//   const ChatScreen({super.key});

//   @override
//   State<ChatScreen> createState() => _ChatScreenState();
// }

// class _ChatScreenState extends State<ChatScreen> {
//   final TextEditingController _messageController = TextEditingController();
//   final List<String> _messages = [];
//   WebSocketChannel? _registerChannel;
//   WebSocketChannel? _sendChannel;
//   WebSocketChannel? _publicChannel;

//   @override
//   void initState() {
//     super.initState();
//     _connectWebSocket();
//   }

//   void _connectWebSocket() {
//     _registerChannel = IOWebSocketChannel.connect(
//         'ws://13.201.237.163:8087/app/chat.register');
//     _sendChannel =
//         IOWebSocketChannel.connect('ws://13.201.237.163:8087/app/chat.send');
//     _publicChannel =
//         IOWebSocketChannel.connect('ws://13.201.237.163:8087/topic/public');

//     _publicChannel?.stream.listen((message) {
//       setState(() {
//         _messages.add(message.toString());
//       });
//     });
//   }

//   void _sendMessage() {
//     if (_messageController.text.isNotEmpty) {
//       _sendChannel?.sink.add(_messageController.text);
//       _messageController.clear();
//     }
//   }

//   @override
//   void dispose() {
//     _registerChannel?.sink.close();
//     _sendChannel?.sink.close();
//     _publicChannel?.sink.close();
//     _messageController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('WebSocket Chat'),
//       ),
//       body: Column(
//         children: [
//           Expanded(
//             child: ListView.builder(
//               itemCount: _messages.length,
//               itemBuilder: (context, index) {
//                 return ListTile(
//                   title: Text(_messages[index]),
//                 );
//               },
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.all(8.0),
//             child: Row(
//               children: [
//                 Expanded(
//                   child: TextField(
//                     controller: _messageController,
//                     decoration: const InputDecoration(
//                       hintText: 'Enter your message',
//                       border: OutlineInputBorder(),
//                     ),
//                   ),
//                 ),
//                 const SizedBox(width: 8),
//                 ElevatedButton(
//                   onPressed: _sendMessage,
//                   child: const Text('Send'),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

import 'package:chat_page_application/localdatabase_screen.dart';
import 'package:chat_page_application/user_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  DatabaseHelper.instance;
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: UserSelectionScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
