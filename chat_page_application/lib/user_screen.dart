import 'package:chat_page_application/club_list.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';

class UserSelectionScreen extends StatefulWidget {
  const UserSelectionScreen({super.key});

  @override
  State<UserSelectionScreen> createState() => _UserSelectionScreenState();
}

class _UserSelectionScreenState extends State<UserSelectionScreen> {
  final TextEditingController idController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController clubController = TextEditingController();

  @override
  void dispose() {
    idController.dispose();
    nameController.dispose();
    clubController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Select or Enter User')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: clubController,
              decoration: const InputDecoration(
                labelText: 'Enter Club ID',
              ),
            ),
            TextField(
              controller: idController,
              decoration: const InputDecoration(
                labelText: 'Enter User ID',
              ),
            ),
            TextField(
              controller: nameController,
              decoration: const InputDecoration(
                labelText: 'Enter Your Name',
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: () {
                if (idController.text.isNotEmpty &&
                    nameController.text.isNotEmpty) {
                  final user = ChatUser(
                    id: idController.text,
                    firstName: nameController.text,
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ClubListScreen(
                        loggedInUser: user,
                        clubId: clubController.text,
                      ),
                    ),
                  );
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(content: Text("Please enter both fields")),
                  );
                }
              },
              child: const Text('Start Chatting'),
            ),
          ],
        ),
      ),
    );
  }
}
