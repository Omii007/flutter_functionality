import 'package:chat_page_application/localdatabase_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:flutter/material.dart';

import 'firebase_chat.dart';

class ClubListScreen extends StatefulWidget {
  final ChatUser loggedInUser;
  final String? clubId;
  const ClubListScreen({
    super.key,
    this.clubId,
    required this.loggedInUser,
  });

  @override
  State<ClubListScreen> createState() => _ClubListScreenState();
}

class Club {
  final String? clubName;
  Club({
    this.clubName,
  });
}

class _ClubListScreenState extends State<ClubListScreen> {
  Stream<QuerySnapshot> getGroupsStream() {
    final name = clubName.map((e) => e.clubName).toList();
    print("ClubNAme : $name");
    return FirebaseFirestore.instance
        .collection('messages')
        .where(
          'bookClubId',
          whereIn: name,
        )
        .orderBy('createdAt', descending: true)
        .snapshots();
  }

  List<Club> clubName = [
    Club(
      clubName: '1',
    ),
    Club(
      clubName: '2',
    ),
    Club(
      clubName: '3',
    ),
  ];

  void checkLatestMessage() {}

  @override
  void initState() {
    // clubName.add(widget.clubId ?? '');
    receiveMetadata();
    super.initState();
  }

  bool isSeenMessage = false;
  Map<String, bool> notificationStatus = {}; // To track notification status
  Future<void> receiveMetadata() async {
    FirebaseFirestore.instance
        .collection('messages')
        .snapshots()
        .listen((snapshot) async {
      Map<String, bool> tempNotificationStatus = {};

      for (var doc in snapshot.docs) {
        final String bookClubId = doc.data()['bookClubId'] ?? '';
        final Timestamp? createdAtTimestamp = doc.data()['createdAt'];
        final DateTime? firebaseCreatedAt = createdAtTimestamp?.toDate();
        final String? firebaseCreatedAtIso =
            firebaseCreatedAt?.toIso8601String();

        final localData =
            await DatabaseHelper.instance.getClubMetadata(bookClubId);
        final String? localCreatedAt = localData?['createdAt'];

        // Check if notification should be shown for this club
        tempNotificationStatus[bookClubId] =
            firebaseCreatedAtIso != localCreatedAt;
      }

      setState(() {
        notificationStatus = tempNotificationStatus;
      });
    });
  }
  // Future<void> receiveMetadata() async {
  //   FirebaseFirestore.instance
  //       .collection('messages')
  //       .snapshots()
  //       .listen((value) async {
  //     value.docs.map((e) async {
  //       // final date =
  //       //     DateTime.fromMillisecondsSinceEpoch((e.data()['createdAt'] * 1000));
  //       // print('Date: ${date}');
  //       final Timestamp? createdAtTimestamp = e.data()['createdAt'];
  //       final DateTime? createdAt = createdAtTimestamp?.toDate();
  //       final String? finalCreatedAt = createdAt?.toIso8601String();
  //       print("Latest TimeStamp : ${createdAt?.toIso8601String()}");
  //       final data = await DatabaseHelper.instance
  //           .getClubMetadata(e.data()['bookClubId']);
  //       print("LocalDatabase CreatedAt: ${data?['createdAt']}");
  //       if (finalCreatedAt != data?['createdAt']) {
  //         isSeenMessage = true;
  //       }
  //     }).toList();
  //     // Safely retrieve and handle `createdAt` field

  //     // await DatabaseHelper.instance.insertOrUpdateClubMetadata(
  //     //     value.data()?['bookClubId'], (createdAt ?? DateTime.now()));
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Club List',
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
      ),
      body: StreamBuilder(
          stream: getGroupsStream(),
          builder: (context, snapShot) {
            if (snapShot.connectionState == ConnectionState.waiting) {
              return const Center(child: CircularProgressIndicator());
            }
            if (snapShot.hasError) {
              return const Center(child: Text('Error loading clubs.'));
            }
            if (!snapShot.hasData || snapShot.data?.docs.isEmpty == true) {
              return const Center(child: Text('No clubs found.'));
            }
            final clubs = snapShot.data?.docs;
            print('Clubs : $clubs');
            return ListView.builder(
              itemCount: clubs?.length,
              itemBuilder: (context, index) {
                final String bookClubId = clubs?[index]['bookClubId'] ?? '';
                return Stack(
                  clipBehavior: Clip.none,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10.0),
                      child: ListTile(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => FireBaseChat(
                                loggedInUser: widget.loggedInUser,
                                clubId: clubs?[index]['bookClubId'] ?? '',
                              ),
                            ),
                          );
                        },
                        shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: const BorderSide(
                            color: Colors.black,
                            width: 1.5,
                          ),
                        ),
                        title: Text(
                          clubs?[index]['bookClubId'] ?? '',
                          style: const TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    notificationStatus[bookClubId] == true
                        ? const Positioned(
                            left: 0,
                            right: 20,
                            top: 15,
                            child: Icon(
                              Icons.notification_add_outlined,
                              size: 25,
                            ),
                          )
                        : const SizedBox.shrink(),
                  ],
                );
              },
            );
          }),
    );
  }
}
