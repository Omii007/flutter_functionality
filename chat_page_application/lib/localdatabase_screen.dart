import 'package:dash_chat_2/dash_chat_2.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static const _databaseName = "clubdatbase.db";
  static const _databaseVersion = 1;
  static const table = 'messages';

  static const columnId = '_id';
  static const columnText = 'text';
  static const clubId = 'club_Id';
  static const columnUserId = 'user_id';
  static const columnUserName = 'user_name';
  static const columnCreatedAt = 'createdAt';

  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database? _database;

  Future<Database> get database async {
    if (_database != null) return _database!;
    _database = await _initDatabase();
    return _database!;
  }

  Future<Database> _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE $table (
        $columnId INTEGER PRIMARY KEY AUTOINCREMENT,
        $columnText TEXT NOT NULL,
        $clubId TEXT NOT NULL,
        $columnUserId TEXT NOT NULL,
        $columnUserName TEXT,
        $columnCreatedAt TEXT NOT NULL
      )
    ''');

    // Create club metadata table
    await db.execute('''
    CREATE TABLE club_metadata (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      clubId TEXT NOT NULL UNIQUE,
      createdAt TEXT NOT NULL
    )
  ''');
  }

  Future<int> insertMessage(ChatMessage message, String clubIds) async {
    Database db = await database;
    return await db.insert(
        table,
        {
          columnText: message.text,
          clubId: clubIds,
          columnUserId: message.user.id,
          columnUserName: message.user.firstName ?? '',
          columnCreatedAt: message.createdAt.toIso8601String(),
        },
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<void> insertOrUpdateClubMetadata(
      String clubId, DateTime createdAt) async {
    final db = await database;

    // Insert or update the club metadata
    await db.insert(
      'club_metadata',
      {
        'clubId': clubId,
        'createdAt': createdAt.toIso8601String(),
      },
      conflictAlgorithm:
          ConflictAlgorithm.replace, // Replace if clubId already exists
    );
  }

  Future<Map<String, dynamic>?> getClubMetadata(String clubId) async {
    final db = await database;

    final List<Map<String, dynamic>> results = await db.query(
      'club_metadata',
      where: 'clubId = ?',
      whereArgs: [clubId],
    );

    if (results.isNotEmpty) {
      return results.first;
    }
    return null;
  }

  Future<List<ChatMessage>> getMessages() async {
    Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query(table);

    return List.generate(maps.length, (i) {
      return ChatMessage(
        text: maps[i][columnText],
        user: ChatUser(
          id: maps[i][columnUserId],
          firstName: maps[i][columnUserName],
        ),
        createdAt: DateTime.parse(maps[i][columnCreatedAt]),
      );
    });
  }
}
