
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ScrollPage extends StatefulWidget {
  const ScrollPage({super.key});

  @override
  State createState()=> _ScrollPageState();
}

class _ScrollPageState extends State {

  // List of Images
  List<String> imageList = [
    "assets/images/allround.png",
    "assets/images/bat1.png",
    "assets/images/bowler.png",
    "assets/images/photo.jpg",
  ];
  final CarouselController _carouselController = CarouselController();
  int currentImage = 0;

  // Function indicates current page index
  Widget indicator(int index){
    return Container(
      margin: const EdgeInsets.all(5),
      width: currentImage == index? 20:10,
      height: 10,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: currentImage == index? 
            Colors.black:Colors.grey,
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("CAROUSEL SLIDER",
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
            color: Colors.black,
          ),
        ),
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          // CarouselSlider builder for scroll images automatic
          CarouselSlider.builder(
            carouselController: _carouselController,
            itemCount: imageList.length, 
            itemBuilder: (BuildContext context,int index,int realIndex){
              return Container(
                margin: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: const [
                    BoxShadow(
                      offset: Offset(3, 3),
                      spreadRadius: 3,
                      blurRadius: 3,
                      color: Colors.black54,
                    ),
                  ],
                  image: DecorationImage(
                    image: AssetImage(imageList[index]),
                    fit: BoxFit.cover,
                    filterQuality: FilterQuality.high,
                  ),
                ),
              );
            }, 
            // CarouselOptions used to features provided to CarouselSlider
            options: CarouselOptions(
              
              autoPlay: true,
              aspectRatio: 1,
              viewportFraction: 1,
              onPageChanged: (index, reason) {
                setState(() {
                  currentImage = index;

                });
              },
            ),
          ),
          // shows indicators 
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: 
              List.generate(imageList.length, (index){
                return InkWell(
                  onTap: () {
                    _carouselController.animateToPage(
                      index,
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeIn,  
                    );
                  },
                  child: indicator(index));
              }),
            
          ),
        ],
      ),
    );
  }
}