import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetxBuilderScreen extends StatefulWidget {
  const GetxBuilderScreen({super.key});

  @override
  State createState() => _GetxBuilderScreenstate();
}

class _GetxBuilderScreenstate extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "GetX",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: GetBuilder(
        init: Player(
          jerNo: 8,
          iplTeam: "RR",
          playerName: "Jadduu",
        ),
        builder: (controller) {
          return Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Player Name :${controller.playerName}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "JerNo : ${controller.jerNo}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "JerNo : ${controller.iplTeam}",
                  style: const TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  onPressed: () {
                    controller.updatePlayerDetails("CSK");
                  },
                  child: const Text(
                    "Update",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class Player extends GetxController {
  String playerName;
  int jerNo;
  String iplTeam;

  Player({
    required this.playerName,
    required this.jerNo,
    required this.iplTeam,
  });

  void updatePlayerDetails(String iplTeam) {
    this.iplTeam = iplTeam;
    update();
  }
}
