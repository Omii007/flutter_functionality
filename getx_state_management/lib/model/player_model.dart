class PlayerData {
  String playerName;
  int jerNo;
  String team;
  int runs;

  PlayerData({
    required this.playerName,
    required this.jerNo,
    required this.team,
    required this.runs,
  });
}
