import 'package:get/get.dart';
import 'package:getx_state_management/model/player_model.dart';

class PlayerController extends GetxController {
  /// RX => REACTIVE GETX (Rx<T>)
  /// Obx => OBSERVABLE GETX

  Rx<PlayerData> player = PlayerData(
    playerName: "Virat Kohli",
    jerNo: 18,
    team: "RCB",
    runs: 14000,
  ).obs;

  void updatePlayerData(int runs) {
    player.update((obj) {
      obj!.runs = runs;
    });
  }
}
