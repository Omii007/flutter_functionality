import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetXScreen extends StatefulWidget {
  const GetXScreen({
    super.key,
  });

  @override
  State createState() => _GetxScreenState();
}

class _GetxScreenState extends State {
  EmpController empController = Get.put(EmpController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "GetX",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.amber,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            /// UPDATE() METHOD BUILD METHOD LA CALL KARTE BUT FIRST OBJECT MADHIL LOCAL DATA CHANGE ZALA NAHI MNUN UI CHANGE ZALA NAHI. BUT DATA UPDATE HOTO PN UI REFLECT HOTA NAHI.
            Text(
              empController.empName,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              "${empController.empId}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              empController.projDom,
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              onPressed: () {
                empController.updateProjDom("IT");
              },
              style: const ButtonStyle(
                backgroundColor: WidgetStatePropertyAll(
                  Colors.blue,
                ),
              ),
              child: const Text(
                "Update",
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class EmpController extends GetxController {
  String empName = "Kanha";
  int empId = 20;
  String projDom = "HealthCare";

  void updateProjDom(String projDom) {
    this.projDom = projDom;
    update();
  }
}
